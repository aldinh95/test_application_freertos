################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../App/Src/thread1.c \
../App/Src/thread2.c \
../App/Src/thread3.c \
../App/Src/thread4.c \
../App/Src/uart.c 

OBJS += \
./App/Src/thread1.o \
./App/Src/thread2.o \
./App/Src/thread3.o \
./App/Src/thread4.o \
./App/Src/uart.o 

C_DEPS += \
./App/Src/thread1.d \
./App/Src/thread2.d \
./App/Src/thread3.d \
./App/Src/thread4.d \
./App/Src/uart.d 


# Each subdirectory must supply rules for building sources it contributes
App/Src/%.o: ../App/Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DUSE_HAL_DRIVER -DSTM32F103xB -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Inc" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Drivers/STM32F1xx_HAL_Driver/Inc" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Middlewares/Third_Party/FreeRTOS/Source/include" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/Drivers/CMSIS/Include" -I"D:/Praktikum/Projekte/Test_Application_FREERTOS/App/Inc"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


