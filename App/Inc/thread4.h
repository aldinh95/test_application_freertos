/*
 * thread4.h
 *
 *  Created on: 07.07.2020
 *      Author: Acer-NitroBE
 */

#ifndef THREAD4_H_
#define THREAD4_H_

#include "uart.h"
#include "cmsis_os2.h"

extern osMutexId_t uart_mutexHandle;

void StartTask04();

#endif /* THREAD3_H_ */
