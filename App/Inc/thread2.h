/*
 * thread2.h
 *
 *  Created on: 07.07.2020
 *      Author: Acer-NitroBE
 */

#ifndef THREAD2_H_
#define THREAD2_H_

#include "uart.h"
#include "cmsis_os2.h"

extern osMutexId_t uart_mutexHandle;

void StartTask02();

#endif /* THREAD2_H_ */
