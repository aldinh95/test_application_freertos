/*
 * thread3.h
 *
 *  Created on: 07.07.2020
 *      Author: Acer-NitroBE
 */

#ifndef THREAD3_H_
#define THREAD3_H_

#include "uart.h"
#include "cmsis_os2.h"

extern osMutexId_t uart_mutexHandle;

void StartTask03();

#endif /* THREAD3_H_ */
