/*
 * uart.h
 *
 *  Created on: 07.07.2020
 *      Author: Acer-NitroBE
 */

#ifndef UART_H_
#define UART_H_

#include "stm32f1xx_hal.h"

extern UART_HandleTypeDef huart1;


void uart_getChar(uint8_t *data, uint16_t timeout);
void uart_sendChar(uint8_t data);
void uart_sendString(uint8_t* data);


#endif /* UART_H_ */
