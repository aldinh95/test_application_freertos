/*
 * thread1.h
 *
 *  Created on: 07.07.2020
 *      Author: Acer-NitroBE
 */

#ifndef THREAD1_H_
#define THREAD1_H_

#include "uart.h"
#include "cmsis_os2.h"
//#include "stm32f1xx_hal.h"
//#include "STM32F1xx_HAL_GPIO.H"
#include "main.h"

extern osMutexId_t uart_mutexHandle;

void ownthreadtask1();

#endif /* THREAD1_H_ */
