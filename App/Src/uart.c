/*
 * uart.c
 *
 *  Created on: 07.07.2020
 *      Author: Acer-NitroBE
 */

#include "uart.h"

void uart_getChar(uint8_t *data, uint16_t timeout){
	HAL_UART_Receive(&huart1, data, 1, timeout);

}

void uart_sendChar(uint8_t data){
  /* Make available the UART module. */
  if (HAL_UART_STATE_TIMEOUT == HAL_UART_GetState(&huart1))
  {
    HAL_UART_Abort(&huart1);
  }

  HAL_UART_Transmit(&huart1, &data, 1u, 1000u);

}

void uart_sendString(uint8_t* data){

	  uint16_t length = 0u;

	  /* Calculate the length. */
	  while ('\0' != data[length])
	  {
	    length++;
	  }
	  HAL_UART_Transmit(&huart1, data, length, 100);

}
