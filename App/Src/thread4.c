#include "thread4.h"

void StartTask04(){

	while(1){
	osMutexAcquire(uart_mutexHandle, osWaitForever);

	//clear buffer (2 byte)
	uart_sendString((uint8_t*)"\033[2J");
	uart_sendString((uint8_t*)" \033[H");

	uint8_t clear = 0;

	uart_getChar(&clear, 0);
	uart_getChar(&clear, 0);

	uart_sendString((uint8_t*)"Start Thread 4\n\r");

	uint8_t test = 0;

	uart_getChar(&test, 5000);

	if(test == 0){
		uart_sendString((uint8_t*)"No input\n\r");
	}else{
		uart_sendString((uint8_t*)"Input: ");
		uart_sendChar(test);
		uart_sendString((uint8_t*)"\n\r");
	}

	osDelay(2000);


	uart_sendString((uint8_t*)"End Thread 4\n\r");


	osDelay(1000);

	osMutexRelease(uart_mutexHandle);

	osDelay(100);
	}

}
