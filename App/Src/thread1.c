#include "thread1.h"

void ownthreadtask1() {

	while (1) {
		osMutexAcquire(uart_mutexHandle, osWaitForever);

		//clear buffer (2 byte)
		uart_sendString((uint8_t*) "\033[2J");
		uart_sendString((uint8_t*) "\033[H");

		uint8_t clear = 0;

		uart_getChar(&clear, 0);
		uart_getChar(&clear, 0);

		uart_sendString((uint8_t*) "Start Thread 1\n\r");

		uart_sendString((uint8_t*) "Hello World\n\r");

		uart_sendString((uint8_t*) "End Thread 1\n\r");

		osDelay(2000);

		osMutexRelease(uart_mutexHandle);

		osDelay(100);
	}

}
